<?php
/**
 * @file
 * sankey_diagram_content_type.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function sankey_diagram_content_type_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_body_post'.
  $field_bases['field_body_post'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_body_post',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_sankey_diagram'.
  $field_bases['field_sankey_diagram'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sankey_diagram',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'tablefield',
    'settings' => array(
      'cell_processing' => 0,
      'entity_translation_sync' => FALSE,
      'export' => 0,
      'hide_headers' => 1,
      'lock_values' => 0,
      'restrict_rebuild' => 0,
    ),
    'translatable' => 0,
    'type' => 'tablefield',
  );

  return $field_bases;
}
