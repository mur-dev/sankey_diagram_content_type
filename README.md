# Sankey diagram content type

A content type to hold and display data for a single Sankey diagram.

# Requirements


* [Drupal core](https://www.drupal.org/project/drupal), version 7.0 or higher, and it's *Text* sub-module
* [the Features module](https://www.drupal.org/project/features)
* [the University of Waterloo: Career paths](https://git.uwaterloo.ca/m2parker/uw_career_paths) custom module, which depends on:
    * [The Tablefield module](https://www.drupal.org/project/tablefield)
    * The *D3 Sankey: Table grouping preprocessor* (`d3_sankey_table_group_pp`) sub-module inside [the D3 Sankey module](https://www.drupal.org/project/d3_sankey), which depends on:
        * [The Composer Manager contributed module](https://www.drupal.org/project/composer_manager),
        * [The D3 Sankey contributed module](https://www.drupal.org/project/d3_sankey), which depends on:
            * [The D3 contribtued module](https://www.drupal.org/project/d3)
            * [The Libraries API contributed module](https://www.drupal.org/project/libraries), version 7.x-2.0 or higher
            * [The XAutoload contributed module](https://www.drupal.org/project/xautoload), version 7.x-5.0 or higher
            * [The D3 library](http://d3js.org/), [downloaded](https://github.com/d3/d3/releases/) into `sites/*/libraries`
            * [The d3-sankey library](https://github.com/newrelic-forks/d3-plugins-sankey), [downloaded](https://github.com/newrelic-forks/d3-plugins-sankey/releases) into `sites/*/libraries`
            * [The d3.chart library](https://github.com/misoproject/d3.chart), [downloaded](https://github.com/misoproject/d3.chart/releases) into `sites/*/libraries`
            * [The d3.chart.sankey library](https://github.com/q-m/d3.chart.sankey), [downloaded](https://github.com/q-m/d3.chart.sankey/releases) into `sites/*/libraries`

# Recommended modules

* [The Code Per Node module](https://www.drupal.org/project/cpn), if you want to add custom CSS to a particular Drupal node.

# Installation

1. Download, install, and enable this module and its dependencies. See https://drupal.org/node/895232 for further information.
2. Download and install all JavaScript libraries that this module and its dependencies need. See https://www.drupal.org/node/1440066 for further information.
3. Log in as an administrator. Go to `/admin/people/permissions`. In the *Node* section, set permissions for creating, editing, and deleting *Sankey diagram* content. Click `Save permissions`.

# Configuration

1. Log in as a user who can create Sankey diagram content. Go to `/node/add/sankey-diagram`. Enter a title, body, and upload a CSV file to display. Click `Save`.
