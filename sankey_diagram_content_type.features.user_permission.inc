<?php
/**
 * @file
 * sankey_diagram_content_type.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sankey_diagram_content_type_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create sankey_diagram content'.
  $permissions['create sankey_diagram content'] = array(
    'name' => 'create sankey_diagram content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any sankey_diagram content'.
  $permissions['delete any sankey_diagram content'] = array(
    'name' => 'delete any sankey_diagram content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own sankey_diagram content'.
  $permissions['delete own sankey_diagram content'] = array(
    'name' => 'delete own sankey_diagram content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any sankey_diagram content'.
  $permissions['edit any sankey_diagram content'] = array(
    'name' => 'edit any sankey_diagram content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own sankey_diagram content'.
  $permissions['edit own sankey_diagram content'] = array(
    'name' => 'edit own sankey_diagram content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
